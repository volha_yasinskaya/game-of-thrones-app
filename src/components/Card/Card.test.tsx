import { shallow } from 'enzyme';
import Card, { CardProps } from './Card';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: (key: any) => key }),
}));

describe('<Card />', () => {
  let component: any;

  const cardProps: CardProps = {
    title: 'Test Title',
    items: [
      { label: 'testLabel', value: 'testValue' },
      { label: 'testLabel', value: 'testValue' },
    ],
    toReadMore: { state: 'state', pathname: 'testPathName' },
  };

  beforeEach(() => {
    component = shallow(
      <Card
        title={cardProps.title}
        items={cardProps.items}
        toReadMore={cardProps.toReadMore}
      />
    );
  });

  test('It should mount', () => {
    expect(component.length).toBe(1);
  });

  test('It should render book items array', () => {
    expect(component.find('.card__item').length).toBe(2);
  });

  test('It should render passed title', () => {
    expect(component.find('h2').text()).toBe(cardProps.title);
  });
});
