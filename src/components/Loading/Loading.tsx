import React from 'react';

import './Loading.scss';

const Loading: React.FC = () => <div className="loading"></div>;

export default Loading;
