import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import './i18n';
import './App.scss';

import { AppRouter } from './AppRouter';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';

const App: React.FC = () => (
  <div className="app">
    <ErrorBoundary>
      <Router>
        <AppRouter />
      </Router>
    </ErrorBoundary>
  </div>
);

export default App;
