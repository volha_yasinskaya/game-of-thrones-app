import { toJS } from 'mobx';

export const getItemIdFromURL = (url: string): string => {
  const lastSlashIndex = url.lastIndexOf('/');
  const itemId = url.substring(lastSlashIndex + 1);

  return itemId;
};

export const convertToStringValue = (
  value?: string[] | string | number
): string => {
  if (value) {
    const jsValue = toJS(value);
    return Array.isArray(jsValue) ? jsValue.join(', ') : value.toString();
  }
  return '-';
};
