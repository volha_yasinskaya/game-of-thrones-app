import React, { useCallback, useRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import './BookModal.scss';
import { Character, Book } from '../../common/Types';
import { convertToStringValue } from '../../common/services/Services';
import Loading from '../Loading/Loading';
import moment from 'moment';

interface BookModalProps {
  selectedBook: Book;
  characters: Character[];
  isLoading: boolean;
  hasMore: boolean;
  increasePageNumber: () => void;
  closeModal: (e: any) => void;
}

const BookModal: React.FC<BookModalProps> = React.memo(
  ({
    selectedBook,
    characters,
    isLoading,
    hasMore,
    closeModal,
    increasePageNumber,
  }) => {
    const { t } = useTranslation();

    const [searchTerm, setSearchTerm] = useState('');
    const [filteredCharacters, setFilteredCharacters] = useState(characters);

    useEffect(() => {
      const searchedCharacters = characters.filter((character) =>
        character.name.toLowerCase().includes(searchTerm)
      );
      setFilteredCharacters(searchedCharacters);
    }, [searchTerm, characters]);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      setSearchTerm(event.target.value);
    };

    const onModalContentClick = (e: any) => {
      e.stopPropagation();
    };

    const scrollObserver = useRef<IntersectionObserver>();

    const lastCharacterElRef = useCallback(
      (node) => {
        if (isLoading) {
          return;
        }

        if (scrollObserver) {
          scrollObserver.current && scrollObserver.current.disconnect();
        }

        scrollObserver.current = new IntersectionObserver((entries) => {
          if (entries[0].isIntersecting && hasMore) {
            increasePageNumber();
          }
        });

        if (node) {
          scrollObserver.current.observe(node);
        }
      },
      [isLoading, hasMore]
    );

    const createInfoItem = useCallback(
      (label: string, value: string[] | string | number) => (
        <div className="book-modal__item">
          <span className="book-modal__item-label">{label}:</span>
          <span className="book-modal__item-value">
            {convertToStringValue(value)}
          </span>
        </div>
      ),
      []
    );

    const getBookInfo = useCallback(
      () => (
        <div className="book-modal__sub-section">
          {createInfoItem(
            t('Release Date'),
            moment(selectedBook.released).format('MM/DD/YYYY')
          )}
          {createInfoItem(t('Pages'), selectedBook.numberOfPages)}
          {createInfoItem(t('Authors'), selectedBook.authors)}
        </div>
      ),
      [selectedBook]
    );

    const createCharacterContent = useCallback(
      (character: Character) => (
        <>
          {createInfoItem(t('Name'), character.name)}
          {createInfoItem(t('Gender'), character.gender)}
          {createInfoItem(t('Culture'), character.culture)}
          {createInfoItem(t('Titles'), character.titles)}
          {createInfoItem(t('Born'), character.born)}
          {createInfoItem(t('Allegiances'), character.allegiances)}
          {createInfoItem(t('Aliases'), character.aliases)}
          {createInfoItem(t('Played By'), character.playedBy)}
        </>
      ),
      []
    );

    const getCharacters = useCallback(
      () =>
        filteredCharacters.map((character: Character, idx: number) =>
          filteredCharacters.length === idx + 1 ? (
            <div
              className="book-modal__sub-section"
              key={character.url}
              ref={lastCharacterElRef}>
              {createCharacterContent(character)}
            </div>
          ) : (
            <div className="book-modal__sub-section" key={character.url}>
              {createCharacterContent(character)}
            </div>
          )
        ),
      [selectedBook, filteredCharacters]
    );

    return (
      <div className="book-modal" onClick={closeModal}>
        <div className="book-modal__content" onClick={onModalContentClick}>
          <div className="book-modal__title">
            <h2>{selectedBook.name}</h2>
            <button type="button" onClick={closeModal}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="book-modal__section">{getBookInfo()}</div>
          <div className="book-modal__section">
            <div className="book-modal__characters-title">
              <h3>{t('Characters')}</h3>
              <input
                type="text"
                placeholder={t('Search Character')}
                value={searchTerm}
                onChange={handleChange}
                className="book-modal__search"
              />
            </div>
            {getCharacters()}
            {isLoading && <Loading />}
          </div>
        </div>
      </div>
    );
  }
);

export default BookModal;
