# Game of Thrones App

## What is covered

 - Books page
 - Book details (availible by clicking on 'View Characters' button or by direct link)
 - Search support for both pages

## What wasn't finished
 
 - Typing is not fully covered (any type is used in several places)
 - Not full test coverage


