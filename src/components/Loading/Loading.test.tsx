import { shallow } from 'enzyme';
import Loading from './Loading';

describe('<Loading />', () => {
  let component: any;

  beforeEach(() => {
    component = shallow(<Loading />);
  });

  test('It should mount', () => {
    expect(component.length).toBe(1);
  });
});
