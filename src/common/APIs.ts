import { Book, Character } from './Types';

const BASE_URL = 'https://www.anapioficeandfire.com/api';

export const getRequest =
  <T>(url: string) =>
  async (params?: { id?: string }): Promise<T> => {
    try {
      const getURL = params && params.id ? `${url}/${params.id}` : url;
      const response = await fetch(getURL);

      return response.clone().json();
    } catch (e) {
      throw new Error(e);
    }
  };

export const getBooks = getRequest<Book[]>(`${BASE_URL}/books`);
export const getCharacters = getRequest<Character[]>(`${BASE_URL}/characters`);
export const getAllegiances = getRequest(`${BASE_URL}/allegiances`);
export const getBook = getRequest<Book>(`${BASE_URL}/books`);

