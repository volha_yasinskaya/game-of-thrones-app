export interface Book {
  name: string;
  released: string;
  numberOfPages: number;
  authors: string[];
  url: string;
  isbn?: string;
  characters?: string[];
}

export interface Character {
  url: string;
  name: string;
  gender: string;
  culture: string;
  titles: string[];
  born: string;
  allegiances: string[];
  aliases: string[];
  playedBy: string[];
}