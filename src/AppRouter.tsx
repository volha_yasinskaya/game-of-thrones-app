import React from 'react';
import { Route, useLocation, Switch, Redirect } from 'react-router-dom';

import './i18n';
import './App.scss';

import Books from './pages/Books/Books';
import { BooksStore } from './store/BooksStore';
import BookDetails from './pages/BookDetails/BookDetails';
import NotFound from './pages/NotFound/NotFound';

const booksStore = new BooksStore();

export const AppRouter: React.FC = () => {
  const location = useLocation();

  const background = location.state && (location.state as any).background;

  return (
    <>
      <Switch location={background || location}>
        <Route path="/books" children={<Books store={booksStore} />} exact />
        <Route
          path="/books/:bookId"
          children={<BookDetails store={booksStore} />}
          exact
        />
        <Route path="/not-found" children={<NotFound />} exact />
        <Redirect from="/" to="/books" exact />
        <Redirect from="*" to="/not-found" exact />
      </Switch>

      {/* Show the modal when a background page is set */}
      {background && (
        <Route
          path="/books/:bookId"
          children={<BookDetails store={booksStore} />}
          exact
        />
      )}
    </>
  );
};
