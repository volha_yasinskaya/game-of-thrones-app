import { observable, action, runInAction } from 'mobx';

import { getBooks, getBook, getRequest } from '../common/APIs';
import { getItemIdFromURL } from '../common/services/Services';
import { Book, Character } from '../common/Types';

const PAGINATION_STEP = 20;

export class BooksStore {
  @observable books: Book[] = [];
  @observable selectedBook: Book | undefined = undefined;
  @observable charactersMap: { [url: string]: Character } = {};
  @observable selectedBookCharacters: Character[] = [];
  @observable allegiancesMap: { [url: string]: string } = {};

  @observable charactersPaginationNumber: number = 1;
  @observable hasMoreCharactersToLoad: boolean = false;
  @observable areCharactersLoading: boolean = false;

  @action loadBooks = async (): Promise<void> => {
    const loadedBooks: any = await getBooks();

    runInAction(() => {
      this.books = loadedBooks;
    });
  };

  @action selectBook = async (id: string): Promise<void> => {
    this.charactersPaginationNumber = 1;
    this.selectedBookCharacters = [];

    if (this.books.length) {
      this.selectedBook = this.books.find(
        (book) => getItemIdFromURL(book.url) === id
      );
    } else {
      const loadedBook = await getBook({ id });
      this.selectedBook = loadedBook;
    }
  };

  @action increaseCharactersPageNumber = (): void => {
    this.charactersPaginationNumber += 1;
  };

  @action loadBookCharacters = async (): Promise<void> => {
    runInAction(() => {
      this.areCharactersLoading = true;
    });

    const characters = await Promise.all(
      this.selectedBook!.characters!.slice(
        PAGINATION_STEP * (this.charactersPaginationNumber - 1),
        PAGINATION_STEP * this.charactersPaginationNumber
      ).map(async (link: string) => {
        if (!this.charactersMap[link]) {
          const loadedCharacter = await getRequest<Character>(link)();

          loadedCharacter.allegiances = await Promise.all(
            loadedCharacter.allegiances.map((houseURL: string) =>
              this.loadAlligencies(houseURL)
            )
          );

          this.charactersMap[link] = loadedCharacter;
        }

        return this.charactersMap[link];
      })
    );

    runInAction(() => {
      this.selectedBookCharacters = [
        ...this.selectedBookCharacters,
        ...characters,
      ];

      this.hasMoreCharactersToLoad =
        this.selectedBookCharacters.length <
        this.selectedBook!.characters!.length;

      this.areCharactersLoading = false;
    });
  };

  private loadAlligencies = async (url: string): Promise<string> => {
    if (!this.allegiancesMap[url]) {
      const loadedAlligence = await getRequest<{ name: string }>(url)();

      this.allegiancesMap[url] = loadedAlligence.name;
    }

    return this.allegiancesMap[url];
  };
}
