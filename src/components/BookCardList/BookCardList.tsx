import React, { useMemo, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import moment from 'moment';

import './BookCardList.scss';

import Card, { CardItem } from '../Card/Card';
import { Book } from '../../common/Types';
import {
  convertToStringValue,
  getItemIdFromURL,
} from '../../common/services/Services';

export interface BookCardListProps {
  books: Book[];
}

const BookCardList: React.FC<BookCardListProps> = React.memo(({ books }) => {
  const location = useLocation();

  const { t } = useTranslation();

  const createBookCardItems = useCallback(
    ({ released, numberOfPages, authors }: Book): CardItem[] => [
      {
        label: t('Release Date'),
        value: moment(released).format('MM/DD/YYYY'),
      },
      {
        label: t('Pages'),
        value: convertToStringValue(numberOfPages),
      },
      {
        label: t('Authors'),
        value: convertToStringValue(authors),
      },
    ],
    []
  );

  const bookCards = useMemo(
    () =>
      books.map((book: Book) => {
        const bookItems = createBookCardItems(book);
        const bookId = getItemIdFromURL(book.url);

        return (
          <Card
            title={book.name}
            items={bookItems}
            key={bookId}
            readMoreLabel={t('View Characters')}
            toReadMore={{
              pathname: `/books/${bookId}`,
              state: { background: location },
            }}></Card>
        );
      }),
    [books]
  );

  return <div className="book-card-list">{bookCards}</div>;
});

export default BookCardList;
