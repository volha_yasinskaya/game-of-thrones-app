import { shallow } from 'enzyme';
import BookModal from './BookModal';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: (key: any) => key }),
}));

describe('<BookModal />', () => {
  let component: any;
  let closeModalMock: any;

  const selectedBookMock = {
    name: 'TestName',
    released: '1996-08-01T00:00:00',
    numberOfPages: 798,
    authors: ['TestAuthor1', 'TestAuthor2'],
    url: 'https://www.anapioficeandfire.com/api/books/2',
  };

  const charactersMock = [
    {
      url: 'https://www.anapioficeandfire.com/api/characters/2',
      name: 'Walder',
      gender: 'Male',
      culture: '',
      titles: ['Title'],
      aliases: ['Hodor'],
      allegiances: ['House'],
      playedBy: ['Kristian Nairn'],
      born: '',
    },
    {
      url: 'https://www.anapioficeandfire.com/api/characters/3',
      name: 'Walder',
      gender: 'Male',
      culture: '',
      titles: ['Title2'],
      aliases: ['Hodor'],
      allegiances: ['House'],
      playedBy: ['Kristian Nairn2'],
      born: '',
    },
  ];

  beforeEach(() => {
    closeModalMock = jest.fn();

    component = shallow(
      <BookModal
        selectedBook={selectedBookMock}
        characters={charactersMock}
        isLoading={false}
        hasMore={false}
        closeModal={closeModalMock}
        increasePageNumber={() => {}}
      />
    );
  });

  test('It should mount', () => {
    expect(component.length).toBe(1);
  });

  test('It should call closeModalMock on close button click', () => {
    component.find('.book-modal__title button').simulate('click');
    expect(closeModalMock.mock.calls.length).toEqual(1);
  });

  test('It should call closeModalMock on modal wrapper click', () => {
    component.find('.book-modal').simulate('click');
    expect(closeModalMock.mock.calls.length).toEqual(1);
  });

  test('It should have selected book title', () => {
    expect(component.find('h2').text()).toBe(selectedBookMock.name);
  });
});
