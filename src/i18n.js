import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

const resources = {
  en: {
    translation: {
      More: 'More',
      'Release Date': 'Release Date',
      Pages: 'Pages',
      Authors: 'Authors',
      'View Characters': 'View Characters',
      Characters: 'Characters',
      Name: 'Name',
      Close: 'Close',
      Name: 'Name',
      Gender: 'Gender',
      Culture: 'Culture',
      Titles: 'Titles',
      Born: 'Born',
      Allegiances: 'Allegiances',
      Aliases: 'Aliases',
      'Played By': 'Played By',
    },
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'en',
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
